// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';

let myStatusBarItem: vscode.StatusBarItem;

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

	// The command has been defined in the package.json file
	// Now provide the implementation of the command with registerCommand
	// The commandId parameter must match the command field in package.json
	let disposable = vscode.commands.registerCommand('extension.vscode.icon.all.set', () => {
		// The code you place here will be executed every time your command is executed

		var options: vscode.QuickPickOptions = { matchOnDescription: true };

		vscode.window.showQuickPick(getItems(), options)
			.then(selectedCommand => {
				if (selectedCommand) {
					var inputBoxOptions: vscode.InputBoxOptions = {
						prompt: 'Please input message',
						placeHolder: "input message"
					};
					vscode.window.showInputBox(inputBoxOptions).then(value => {
						if (value && selectedCommand.description) {
							// update configuration
							updateConfiguration(selectedCommand.description, value);
						}
					});
				}
			});
	});

	context.subscriptions.push(disposable);

	// register a command that is invoked when the status bar
	// item is selected
	const myCommandId = 'extension.vscode.icon.all.reset';
	context.subscriptions.push(vscode.commands.registerCommand(myCommandId, updateStatusbar));

	// create a new status bar item that we can now manage
	myStatusBarItem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Right, 100);
	// myStatusBarItem.command = myCommandId;
	myStatusBarItem.tooltip = 'VSCode-Icon-All';
	// update status bar item once at start
	updateStatusbar(true);
	context.subscriptions.push(myStatusBarItem);

	context.subscriptions.push(
		vscode.workspace.onDidChangeConfiguration(configChanged)
	);
}

// this method is called when your extension is deactivated
export function deactivate() { }

function updateStatusbar(loadConfig = false): void {
	if (loadConfig) {
		const myConfig = getWorkspace();
		setIconAndMessage(myConfig.get('icon', 'megaphone'), myConfig.get('message', 'for VSCode-Icon-All'));
	} else {
		setIconAndMessage('megaphone', 'for VSCode-Icon-All');
	}
}

function getWorkspace(): vscode.WorkspaceConfiguration {
	return vscode.workspace.getConfiguration('vscodeIconAll');
}

function updateConfiguration(icon: string, message: string): void {
	const myConfig = getWorkspace();
	myConfig.update('icon', icon, true);
	myConfig.update('message', message, true);

	setIconAndMessage(icon, message);
}

function setIconAndMessage(icon: string, message: string): void {
	myStatusBarItem.hide();
	myStatusBarItem.text = `$(${icon}) ${message}`;
	myStatusBarItem.show();
}

function configChanged(params: vscode.ConfigurationChangeEvent) {
	if (params.affectsConfiguration('vscodeIconAll')) {
		updateStatusbar(true);
	}
}

function getItems(): vscode.QuickPickItem[] {
	const array: Array<vscode.QuickPickItem> = [];
	array.push({ label: '$(alert)', description: "alert" });
	array.push({ label: '$(arrow-down)', description: "arrow-down" });
	array.push({ label: '$(arrow-left)', description: "arrow-left" });
	array.push({ label: '$(arrow-right)', description: "arrow-right" });
	array.push({ label: '$(arrow-small-down)', description: "arrow-small-down" });
	array.push({ label: '$(arrow-small-left)', description: "arrow-small-left" });
	array.push({ label: '$(arrow-small-right)', description: "arrow-small-right" });
	array.push({ label: '$(arrow-small-up)', description: "arrow-small-up" });
	array.push({ label: '$(arrow-up)', description: "arrow-up" });
	array.push({ label: '$(microscope)', description: "microscope" });
	array.push({ label: '$(beaker)', description: "beaker" });
	array.push({ label: '$(bell)', description: "bell" });
	array.push({ label: '$(book)', description: "book" });
	array.push({ label: '$(bookmark)', description: "bookmark" });
	array.push({ label: '$(briefcase)', description: "briefcase" });
	array.push({ label: '$(broadcast)', description: "broadcast" });
	array.push({ label: '$(browser)', description: "browser" });
	array.push({ label: '$(bug)', description: "bug" });
	array.push({ label: '$(calendar)', description: "calendar" });
	array.push({ label: '$(check)', description: "check" });
	array.push({ label: '$(checklist)', description: "checklist" });
	array.push({ label: '$(chevron-down)', description: "chevron-down" });
	array.push({ label: '$(chevron-left)', description: "chevron-left" });
	array.push({ label: '$(chevron-right)', description: "chevron-right" });
	array.push({ label: '$(chevron-up)', description: "chevron-up" });
	array.push({ label: '$(circle-slash)', description: "circle-slash" });
	array.push({ label: '$(circuit-board)', description: "circuit-board" });
	array.push({ label: '$(clippy)', description: "clippy" });
	array.push({ label: '$(clock)', description: "clock" });
	array.push({ label: '$(cloud-download)', description: "cloud-download" });
	array.push({ label: '$(cloud-upload)', description: "cloud-upload" });
	array.push({ label: '$(code)', description: "code" });
	array.push({ label: '$(color-mode)', description: "color-mode" });
	array.push({ label: '$(comment-add)', description: "comment-add" });
	array.push({ label: '$(comment)', description: "comment" });
	array.push({ label: '$(comment-discussion)', description: "comment-discussion" });
	array.push({ label: '$(credit-card)', description: "credit-card" });
	array.push({ label: '$(dash)', description: "dash" });
	array.push({ label: '$(dashboard)', description: "dashboard" });
	array.push({ label: '$(database)', description: "database" });
	array.push({ label: '$(clone)', description: "clone" });
	array.push({ label: '$(desktop-download)', description: "desktop-download" });
	array.push({ label: '$(device-camera)', description: "device-camera" });
	array.push({ label: '$(device-camera-video)', description: "device-camera-video" });
	array.push({ label: '$(device-desktop)', description: "device-desktop" });
	array.push({ label: '$(device-mobile)', description: "device-mobile" });
	array.push({ label: '$(diff)', description: "diff" });
	array.push({ label: '$(diff-added)', description: "diff-added" });
	array.push({ label: '$(diff-ignored)', description: "diff-ignored" });
	array.push({ label: '$(diff-modified)', description: "diff-modified" });
	array.push({ label: '$(diff-removed)', description: "diff-removed" });
	array.push({ label: '$(diff-renamed)', description: "diff-renamed" });
	array.push({ label: '$(ellipsis)', description: "ellipsis" });
	array.push({ label: '$(eye-unwatch)', description: "eye-unwatch" });
	array.push({ label: '$(eye-watch)', description: "eye-watch" });
	array.push({ label: '$(eye)', description: "eye" });
	array.push({ label: '$(file-binary)', description: "file-binary" });
	array.push({ label: '$(file-code)', description: "file-code" });
	array.push({ label: '$(file-directory)', description: "file-directory" });
	array.push({ label: '$(file-media)', description: "file-media" });
	array.push({ label: '$(file-pdf)', description: "file-pdf" });
	array.push({ label: '$(file-submodule)', description: "file-submodule" });
	array.push({ label: '$(file-symlink-directory)', description: "file-symlink-directory" });
	array.push({ label: '$(file-symlink-file)', description: "file-symlink-file" });
	array.push({ label: '$(file-text)', description: "file-text" });
	array.push({ label: '$(file-zip)', description: "file-zip" });
	array.push({ label: '$(flame)', description: "flame" });
	array.push({ label: '$(fold)', description: "fold" });
	array.push({ label: '$(gear)', description: "gear" });
	array.push({ label: '$(gift)', description: "gift" });
	array.push({ label: '$(gist)', description: "gist" });
	array.push({ label: '$(gist-secret)', description: "gist-secret" });
	array.push({ label: '$(git-branch-create)', description: "git-branch-create" });
	array.push({ label: '$(git-branch-delete)', description: "git-branch-delete" });
	array.push({ label: '$(git-branch)', description: "git-branch" });
	array.push({ label: '$(git-commit)', description: "git-commit" });
	array.push({ label: '$(git-compare)', description: "git-compare" });
	array.push({ label: '$(git-merge)', description: "git-merge" });
	array.push({ label: '$(git-pull-request-abandoned)', description: "git-pull-request-abandoned" });
	array.push({ label: '$(git-pull-request)', description: "git-pull-request" });
	array.push({ label: '$(globe)', description: "globe" });
	array.push({ label: '$(graph)', description: "graph" });
	array.push({ label: '$(heart)', description: "heart" });
	array.push({ label: '$(history)', description: "history" });
	array.push({ label: '$(home)', description: "home" });
	array.push({ label: '$(horizontal-rule)', description: "horizontal-rule" });
	array.push({ label: '$(hubot)', description: "hubot" });
	array.push({ label: '$(inbox)', description: "inbox" });
	array.push({ label: '$(info)', description: "info" });
	array.push({ label: '$(issue-closed)', description: "issue-closed" });
	array.push({ label: '$(issue-opened)', description: "issue-opened" });
	array.push({ label: '$(issue-reopened)', description: "issue-reopened" });
	array.push({ label: '$(jersey)', description: "jersey" });
	array.push({ label: '$(key)', description: "key" });
	array.push({ label: '$(keyboard)', description: "keyboard" });
	array.push({ label: '$(law)', description: "law" });
	array.push({ label: '$(light-bulb)', description: "light-bulb" });
	array.push({ label: '$(link)', description: "link" });
	array.push({ label: '$(link-external)', description: "link-external" });
	array.push({ label: '$(list-ordered)', description: "list-ordered" });
	array.push({ label: '$(list-unordered)', description: "list-unordered" });
	array.push({ label: '$(location)', description: "location" });
	array.push({ label: '$(gist-private)', description: "gist-private" });
	array.push({ label: '$(mirror-private)', description: "mirror-private" });
	array.push({ label: '$(git-fork-private)', description: "git-fork-private" });
	array.push({ label: '$(lock)', description: "lock" });
	array.push({ label: '$(logo-github)', description: "logo-github" });
	array.push({ label: '$(mail)', description: "mail" });
	array.push({ label: '$(mail-read)', description: "mail-read" });
	array.push({ label: '$(mail-reply)', description: "mail-reply" });
	array.push({ label: '$(mark-github)', description: "mark-github" });
	array.push({ label: '$(markdown)', description: "markdown" });
	array.push({ label: '$(megaphone)', description: "megaphone" });
	array.push({ label: '$(mention)', description: "mention" });
	array.push({ label: '$(milestone)', description: "milestone" });
	array.push({ label: '$(mirror-public)', description: "mirror-public" });
	array.push({ label: '$(mirror)', description: "mirror" });
	array.push({ label: '$(mortar-board)', description: "mortar-board" });
	array.push({ label: '$(mute)', description: "mute" });
	array.push({ label: '$(no-newline)', description: "no-newline" });
	array.push({ label: '$(octoface)', description: "octoface" });
	array.push({ label: '$(organization)', description: "organization" });
	array.push({ label: '$(package)', description: "package" });
	array.push({ label: '$(paintcan)', description: "paintcan" });
	array.push({ label: '$(pencil)', description: "pencil" });
	array.push({ label: '$(person-add)', description: "person-add" });
	array.push({ label: '$(person-follow)', description: "person-follow" });
	array.push({ label: '$(person)', description: "person" });
	array.push({ label: '$(pin)', description: "pin" });
	array.push({ label: '$(plug)', description: "plug" });
	array.push({ label: '$(repo-create)', description: "repo-create" });
	array.push({ label: '$(gist-new)', description: "gist-new" });
	array.push({ label: '$(file-directory-create)', description: "file-directory-create" });
	array.push({ label: '$(file-add)', description: "file-add" });
	array.push({ label: '$(plus)', description: "plus" });
	array.push({ label: '$(primitive-dot)', description: "primitive-dot" });
	array.push({ label: '$(primitive-square)', description: "primitive-square" });
	array.push({ label: '$(pulse)', description: "pulse" });
	array.push({ label: '$(question)', description: "question" });
	array.push({ label: '$(quote)', description: "quote" });
	array.push({ label: '$(radio-tower)', description: "radio-tower" });
	array.push({ label: '$(repo-delete)', description: "repo-delete" });
	array.push({ label: '$(repo)', description: "repo" });
	array.push({ label: '$(repo-clone)', description: "repo-clone" });
	array.push({ label: '$(repo-force-push)', description: "repo-force-push" });
	array.push({ label: '$(gist-fork)', description: "gist-fork" });
	array.push({ label: '$(repo-forked)', description: "repo-forked" });
	array.push({ label: '$(repo-pull)', description: "repo-pull" });
	array.push({ label: '$(repo-push)', description: "repo-push" });
	array.push({ label: '$(rocket)', description: "rocket" });
	array.push({ label: '$(rss)', description: "rss" });
	array.push({ label: '$(ruby)', description: "ruby" });
	array.push({ label: '$(screen-full)', description: "screen-full" });
	array.push({ label: '$(screen-normal)', description: "screen-normal" });
	array.push({ label: '$(search-save)', description: "search-save" });
	array.push({ label: '$(search)', description: "search" });
	array.push({ label: '$(server)', description: "server" });
	array.push({ label: '$(settings)', description: "settings" });
	array.push({ label: '$(shield)', description: "shield" });
	array.push({ label: '$(log-in)', description: "log-in" });
	array.push({ label: '$(sign-in)', description: "sign-in" });
	array.push({ label: '$(log-out)', description: "log-out" });
	array.push({ label: '$(sign-out)', description: "sign-out" });
	array.push({ label: '$(squirrel)', description: "squirrel" });
	array.push({ label: '$(star-add)', description: "star-add" });
	array.push({ label: '$(star-delete)', description: "star-delete" });
	array.push({ label: '$(star)', description: "star" });
	array.push({ label: '$(stop)', description: "stop" });
	array.push({ label: '$(repo-sync)', description: "repo-sync" });
	array.push({ label: '$(sync)', description: "sync" });
	array.push({ label: '$(tag-remove)', description: "tag-remove" });
	array.push({ label: '$(tag-add)', description: "tag-add" });
	array.push({ label: '$(tag)', description: "tag" });
	array.push({ label: '$(telescope)', description: "telescope" });
	array.push({ label: '$(terminal)', description: "terminal" });
	array.push({ label: '$(three-bars)', description: "three-bars" });
	array.push({ label: '$(thumbsdown)', description: "thumbsdown" });
	array.push({ label: '$(thumbsup)', description: "thumbsup" });
	array.push({ label: '$(tools)', description: "tools" });
	array.push({ label: '$(trashcan)', description: "trashcan" });
	array.push({ label: '$(triangle-down)', description: "triangle-down" });
	array.push({ label: '$(triangle-left)', description: "triangle-left" });
	array.push({ label: '$(triangle-right)', description: "triangle-right" });
	array.push({ label: '$(triangle-up)', description: "triangle-up" });
	array.push({ label: '$(unfold)', description: "unfold" });
	array.push({ label: '$(unmute)', description: "unmute" });
	array.push({ label: '$(versions)', description: "versions" });
	array.push({ label: '$(watch)', description: "watch" });
	array.push({ label: '$(remove-close)', description: "remove-close" });
	array.push({ label: '$(x)', description: "x" });
	array.push({ label: '$(zap)', description: "zap" });

	return array;
}