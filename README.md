# VSCode Icon All

[![](https://vsmarketplacebadge.apphb.com/version/transnano.vscode-icon-all.svg)](https://marketplace.visualstudio.com/items?itemName=transnano.vscode-icon-all)

This extension makes your status bar cool.

## Features

In the initial state, ":speaker: for VSCode-Icon-All" is displayed as shown below, but you can customize it as you like.

![default](https://gitlab.com/transnano/vscode-icon-all/raw/master/images/statusbar-default.png)

You can call the command as follows.

![commands](https://gitlab.com/transnano/vscode-icon-all/raw/master/images/commands.png)

Select "Set new icon and description".

![icons](https://gitlab.com/transnano/vscode-icon-all/raw/master/images/icons.png)

Select the icon to display in the status bar.

![message](https://gitlab.com/transnano/vscode-icon-all/raw/master/images/message.png)

Enter the message to be displayed in the status bar.

![customized](https://gitlab.com/transnano/vscode-icon-all/raw/master/images/customized-statusbar.png)

Then you can customize the status bar to your liking as follows.

## Requirements

There is nothing.

## Extension Settings

* `vscodeIconAll.icon`: Customize the icon shown on statusbar
* `vscodeIconAll.message`: Customize the message shown on statusbar

## Known Issues

Calling out known issues can help limit users opening duplicate issues against your extension.

## Release Notes

Update infomation.

Ref: [CHANGELOG.md](https://gitlab.com/transnano/vscode-icon-all/blob/master/CHANGELOG.md)
