# Change Log
All notable changes to the "vscode-icon-all" extension will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.

## [0.0.4]
- Fix configuration

## [0.0.3]
- Feature configuration

## [0.0.2]
- Update README image

## [0.0.1]
- Initial release